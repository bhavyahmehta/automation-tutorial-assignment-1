import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Main {
	
	private static WebDriver driver;
	private static WebDriverWait wait;
	private static final int DELAY_SECONDS = 10;
	private static final String CHROME_DRIVER_KEY = "webdriver.chrome.driver";
	private static final String DRIVER_PATH = "D:\\Eclipse\\Webdrivers\\chromedriver.exe";

	public static void main(String[] arr) {
		try {
			initDriver();
			wait = new WebDriverWait(driver, DELAY_SECONDS);
			enterOrigin("Lon", "LHR");
			enterDestination("Ind", "Delhi");
			enterDates(tomorrow(), dayAfterTomorrow());
			searchResults();
		} finally {
			driver.quit();
		}

	}

	private static void initDriver() {
		File file = new File(DRIVER_PATH);
		System.setProperty(CHROME_DRIVER_KEY, file.getAbsolutePath());
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.navigate().to("https://www.kayak.co.in/flights");
	}

	private static void enterOrigin(String entered, String select) {
		driver.findElement(By.cssSelector(".primary-content [id*='origin-airport-display-inner']")).click();
		WebElement origin = driver.findElement(By.name("origin"));
		wait.until(ExpectedConditions.elementToBeClickable(origin));
		origin.clear();
		origin.click();
		origin.sendKeys(entered);
		wait.until(ExpectedConditions
				.presenceOfElementLocated(By.xpath("//*[contains(@id,'origin-airport-smartbox-dropdown')]")));
		WebElement originDropdown = driver
				.findElement(By.xpath("//*[contains(@id,'origin-airport-smartbox-dropdown')]"));		
		List<WebElement> originList = originDropdown.findElements(By.tagName("li"));
		java.util.Iterator<WebElement> originItems = originList.iterator();
		while (originItems.hasNext()) {
			WebElement row = originItems.next();
			wait.until(ExpectedConditions.elementToBeClickable(row));//
			String name = row.getAttribute("data-short-name"); //
			if (name != null && name.contains(select)) {
				System.out.println(row.getAttribute("data-short-name"));
				row.click();
				break;
			}
		}
	}

	private static void enterDestination(String entered, String select) {
		WebElement destination = driver.findElement(By.name("destination"));
		wait.until(ExpectedConditions.elementToBeClickable(destination));//
		destination.clear();
		destination.click();
		destination.sendKeys(entered);
		wait.until(ExpectedConditions
				.presenceOfElementLocated(By.xpath("//*[contains(@id,'destination-airport-smartbox-dropdown')]")));
		WebElement destinationDropdown = driver
				.findElement(By.xpath("//*[contains(@id,'destination-airport-smartbox-dropdown')]"));
		List<WebElement> destinationList = destinationDropdown.findElements(By.tagName("li"));
		wait.until(ExpectedConditions.visibilityOfAllElements(destinationList));
		java.util.Iterator<WebElement> destinationItems = destinationList.iterator();
		while (destinationItems.hasNext()) {
			WebElement row = destinationItems.next();
			wait.until(ExpectedConditions.elementToBeClickable(row));
			String name = row.getAttribute("data-short-name");
			if (name != null && name.contains(select)) {
				System.out.println(row.getAttribute("data-short-name"));
				row.click();
				break;
			}
		}
	}

	private static void enterDates(String depart, String _return) {
		WebElement departure_date = driver.findElement(By.xpath("//*[contains(@id,'dateRangeInput-display-start')]"));
		wait.until(ExpectedConditions.elementToBeClickable(departure_date));
		departure_date.click();		
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[contains(@id,'cal-monthsGrid')]")));
		WebElement element = driver.findElement(By.xpath("//*[contains(@id,'cal-monthsGrid')]"));
		wait.until(ExpectedConditions.elementToBeClickable(element));
		element.findElement(By.xpath("//*[@aria-label='" + depart + "']")).click();
		element.findElement(By.xpath("//*[@aria-label='" + _return + "']")).click();
	}

	private static void searchResults() {
		WebElement search = driver.findElement(By.xpath("//*[contains(@id,'submit')]"));
		search.click();
	}

	private static String tomorrow() {
		Calendar c = Calendar.getInstance();
		c.add(Calendar.DATE, 1);
		return new SimpleDateFormat("dd/MM/yyyy").format(c.getTime());
	}

	private static String dayAfterTomorrow() {
		Calendar c = Calendar.getInstance();
		c.add(Calendar.DATE, 2);
		return new SimpleDateFormat("dd/MM/yyyy").format(c.getTime());
	}

}
